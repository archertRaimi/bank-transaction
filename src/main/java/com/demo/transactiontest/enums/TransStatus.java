package com.demo.transactiontest.enums;

public enum TransStatus {
	PENDING, SETTLED, FUTURE, INVALID
}
