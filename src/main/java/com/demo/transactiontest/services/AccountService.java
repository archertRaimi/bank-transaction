package com.demo.transactiontest.services;

import com.demo.transactiontest.model.Account;

public interface AccountService {

	public Account findAccountByIban(String iban);
	
	public void saveAccount(Account account);
	
	public void loadAccount();
}
