package com.demo.transactiontest.services;

import java.util.List;

import com.demo.transactiontest.model.TransactionTest;
import com.demo.transactiontest.requests.TransactionIbanOrderRequest;
import com.demo.transactiontest.requests.TransactionRequest;
import com.demo.transactiontest.requests.TransactionStatusRequest;
import com.demo.transactiontest.responses.TransactionResponse;
import com.demo.transactiontest.responses.TransactionStatusResponse;


public interface TransactionTestService {

	public void addTransaction(TransactionRequest transactionTestRequest);
	
	public List<TransactionResponse> findTransactionsByIban(TransactionIbanOrderRequest transIbanOrderRequest);
 
	public List<TransactionTest> getAll();

	public TransactionStatusResponse getStatusTransaction(TransactionStatusRequest transStatusRequest);
}
