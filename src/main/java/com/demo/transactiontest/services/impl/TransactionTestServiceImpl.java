package com.demo.transactiontest.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.demo.transactiontest.exception.TransactionRegisteredBadRequest;
import com.demo.transactiontest.messages.TransactionTestMessages;
import com.demo.transactiontest.model.Account;
import com.demo.transactiontest.model.TransactionTest;
import com.demo.transactiontest.repositories.TransactionTestRepository;
import com.demo.transactiontest.requests.TransactionIbanOrderRequest;
import com.demo.transactiontest.requests.TransactionRequest;
import com.demo.transactiontest.requests.TransactionStatusRequest;
import com.demo.transactiontest.responses.TransactionResponse;
import com.demo.transactiontest.responses.TransactionStatusResponse;
import com.demo.transactiontest.services.AccountService;
import com.demo.transactiontest.services.TransactionTestService;
import com.demo.transactiontest.services.helper.TransactionTestHelper;
import com.demo.transactiontest.services.helper.TransactionTestStatusHelper;

@Service
public class TransactionTestServiceImpl implements TransactionTestService {

	@Autowired
	private TransactionTestRepository transRepository;
	
	@Autowired
	private ConversionService conversionService;
	
	@Autowired
	private TransactionTestHelper transHelper;
	
	@Autowired
	private TransactionTestStatusHelper transStatusHelper;
	
	@Autowired
	private AccountService accountService;
	
	@Override
	public void addTransaction(TransactionRequest transactionRequest) {
		
		checkTransaction(transactionRequest.getReference());
		
		Account account = accountService.findAccountByIban(transactionRequest.getAccount_iban());
		BigDecimal amountWithFeeDiscount = transHelper.discountFee(transactionRequest.getAmount(), transactionRequest.getFee());
		account.setAmount(transHelper.calculationAmount(account.getAmount(),amountWithFeeDiscount));
		TransactionRequest transRequest = transHelper.setRandomReferenceIfNotExist(transactionRequest);
		TransactionTest transaction = conversionService.convert(transRequest, TransactionTest.class);
		transaction.setAccount(account);
		account.getTransactions().add(transaction);
		accountService.saveAccount(account);
		
	}
	
	private void checkTransaction(String reference) {
		Optional<TransactionTest> transaction = transRepository.findByReference(reference);
		if(transaction.isPresent()) {
			throw new TransactionRegisteredBadRequest(TransactionTestMessages.TRANSACTION_REGISTERED);
		}
		
	}

	@Override
	public List<TransactionResponse> findTransactionsByIban(TransactionIbanOrderRequest transIbanOrderRequest) {
		List<TransactionTest> transactions = new ArrayList<TransactionTest>();
		switch (transIbanOrderRequest.getSort()) {
		case "ASC":
			transactions = transRepository.findTransacctionByIbanOrderAmoutAsc(transIbanOrderRequest.getAccount_iban());
			break;
		case "DESC":
			transactions = transRepository.findTransacctionByIbanOrderAmoutDesc(transIbanOrderRequest.getAccount_iban());
			break;
		}
		List<TransactionResponse> transactionsResponse = transactions.stream().map(e -> conversionService.convert(e, TransactionResponse.class)).collect(Collectors.toList());
		return transactionsResponse;
	}

	@Override
	public List<TransactionTest> getAll() {
		return transRepository.findAll();
	}

	@Override
	public TransactionStatusResponse getStatusTransaction(TransactionStatusRequest transStatusRequest) {
		Optional<TransactionTest> trans = transRepository.findByReference(transStatusRequest.getReference());
		TransactionStatusResponse transResponse = transStatusHelper.mananagedStatusTransaction(trans, transStatusRequest);
		return transResponse;
	}



}
