package com.demo.transactiontest.services.impl;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.transactiontest.exception.AcountUnprocessableException;
import com.demo.transactiontest.messages.TransactionTestMessages;
import com.demo.transactiontest.model.Account;
import com.demo.transactiontest.repositories.AccountRepository;
import com.demo.transactiontest.services.AccountService;

@Service
public class AccountServiceImpl implements AccountService{
	
	@Autowired
	private AccountRepository accountRepository;
	
	public void loadAccount() {
		Account account = new Account();
		account.setIban("ES9820385778983000760236");
		account.setAmount(new BigDecimal(193.38));
		accountRepository.save(account);
	}

	@Override
	public Account findAccountByIban(String iban) {
		Optional<Account> account = accountRepository.findByIban(iban);
		if(!account.isPresent()) {
			throw new AcountUnprocessableException(TransactionTestMessages.ACCOUNT_IBAN_NOT_EXIST);
		}
		return account.get();
	}

	@Override
	public void saveAccount(Account account) {
		accountRepository.save(account);
	}

}
