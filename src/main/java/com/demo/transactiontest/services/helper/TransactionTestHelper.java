package com.demo.transactiontest.services.helper;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;
import org.springframework.stereotype.Service;

import com.demo.transactiontest.exception.AmountUnprocessableException;
import com.demo.transactiontest.messages.TransactionTestMessages;
import com.demo.transactiontest.requests.TransactionRequest;

@Service
public class TransactionTestHelper {

	public TransactionRequest setRandomReferenceIfNotExist(TransactionRequest transactionRequest) {
		Optional<String> reference = Optional.ofNullable(transactionRequest.getReference()).filter(s -> !s.isEmpty());
		if(!reference.isPresent()) {
			transactionRequest.setReference(UUID.randomUUID().toString());
		}
		return transactionRequest;
	}

	public BigDecimal discountFee(BigDecimal amount, BigDecimal fee){
		BigDecimal feedCalculated = checkFeed(fee);
		BigDecimal total = amount.subtract(feedCalculated);
		return total;
	}

	private BigDecimal checkFeed(BigDecimal fee) {
		if(fee == null) {
			return BigDecimal.ZERO;
		}
		return fee;
	}

	public BigDecimal calculationAmount(BigDecimal amount, BigDecimal amountWithFeeDiscount) {
		BigDecimal total = amount.add(amountWithFeeDiscount);
		if(total.compareTo(BigDecimal.ZERO)<0) {
			throw new AmountUnprocessableException(TransactionTestMessages.AMOUNT_UNPROCESSABLE);
		}else {
			return total;
		}
	}



	
}
