package com.demo.transactiontest.services.helper;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;
import org.springframework.stereotype.Service;

import com.demo.transactiontest.enums.TransStatus;
import com.demo.transactiontest.exception.ChannelBadRequestException;
import com.demo.transactiontest.exception.TransactionDateUnprocessableException;
import com.demo.transactiontest.messages.TransactionTestMessages;
import com.demo.transactiontest.model.TransactionTest;
import com.demo.transactiontest.requests.TransactionStatusRequest;
import com.demo.transactiontest.responses.TransactionStatusResponse;

@Service
public class TransactionTestStatusHelper {

	public TransactionStatusResponse mananagedStatusTransaction(Optional<TransactionTest> transaction,
			TransactionStatusRequest transStatusRequest) {
		
		TransactionStatusResponse transBkStatusResponse = new TransactionStatusResponse();
		if(transaction.isPresent()) {
			transBkStatusResponse = buildSatusResponseTransPresent(transaction.get(), transStatusRequest);
		}else {
			transBkStatusResponse = buildStatudResponseTransNotPresent(transStatusRequest.getReference());
		}
		
		return transBkStatusResponse;
	}

	private TransactionStatusResponse buildSatusResponseTransPresent(TransactionTest transaction,
			TransactionStatusRequest transStatusRequest) {
		
		checkDateIsNull(transaction.getDate());
		
		switch (transStatusRequest.getChannel()) {
		case "CLIENT":
			return clientBuildTransactionStatus(transaction);
		case "ATM":
			return atmBuildTransactionStatus(transaction);
		case "INTERNAL":
			return internalBuildTransactionStatus(transaction);
		default:
			throw new ChannelBadRequestException(TransactionTestMessages.CHANNEL_NOT_VALID);
		}
		
	}
	
	private TransactionStatusResponse clientBuildTransactionStatus(TransactionTest transaction) {
		Instant transactionDate = transaction.getDate().toInstant().truncatedTo(ChronoUnit.DAYS);
		Instant today = new Date().toInstant().truncatedTo(ChronoUnit.DAYS);
		
		if(transactionDate.isBefore(today)) {
			return buildStatudResponseClientOrAtmBeforeToday(transaction);
		
		}
		else if(transactionDate.isAfter(today)) {
			return buildStatusReponseClientGreaterToday(transaction);
		
		}
		else {
			return buildStatusReponseClientOrAtmEqualsToday(transaction);
		}
	}
	
	private TransactionStatusResponse atmBuildTransactionStatus(TransactionTest transaction) {
		Instant transactionDate = transaction.getDate().toInstant().truncatedTo(ChronoUnit.DAYS);
		Instant today = new Date().toInstant().truncatedTo(ChronoUnit.DAYS);
		
		if(transactionDate.isBefore(today)) {
			return buildStatudResponseClientOrAtmBeforeToday(transaction);

		}
		else if(transactionDate.isAfter(today)) {
			return buildStatusReponseAtmGreaterToday(transaction);
		
		}
		else {
			return buildStatusReponseClientOrAtmEqualsToday(transaction);
		}
		
	}

	private TransactionStatusResponse internalBuildTransactionStatus(TransactionTest transaction) {
		Instant transactionDate = transaction.getDate().toInstant().truncatedTo(ChronoUnit.DAYS);
		Instant today = new Date().toInstant().truncatedTo(ChronoUnit.DAYS);
		
		if(transactionDate.isBefore(today)) {
			return buildStatudResponseInternalBeforeToday(transaction);
		
		}
		else if(transactionDate.isAfter(today)) {
			return buildStatusReponseInternalGreaterToday(transaction);
		}
		else {
			return buildStatusReponseInternalEqualsToday(transaction);
		}
		
	}

	/* ----TRANSACTION DATE GREATER TODAY ---- */
	private TransactionStatusResponse buildStatusReponseInternalGreaterToday(TransactionTest transaction) {
		TransactionStatusResponse transStatusResponse = new TransactionStatusResponse();
		transStatusResponse.setReference(transaction.getReference());
		transStatusResponse.setStatus(TransStatus.FUTURE);
		transStatusResponse.setAmount(transaction.getAmount());
		transStatusResponse.setFee(transaction.getFee());
		return transStatusResponse;
	}
	private TransactionStatusResponse buildStatusReponseClientGreaterToday(TransactionTest transaction) {
		TransactionStatusResponse transStatusResponse = new TransactionStatusResponse();
		transStatusResponse.setReference(transaction.getReference());
		transStatusResponse.setStatus(TransStatus.FUTURE);
		transStatusResponse.setAmount(transaction.getAmount().subtract(transaction.getFee()));
		return transStatusResponse;
	}
	private TransactionStatusResponse buildStatusReponseAtmGreaterToday(TransactionTest transaction) {
		TransactionStatusResponse transStatusResponse = new TransactionStatusResponse();
		transStatusResponse.setReference(transaction.getReference());
		transStatusResponse.setStatus(TransStatus.PENDING);
		transStatusResponse.setAmount(transaction.getAmount().subtract(transaction.getFee()));
		return transStatusResponse;
	}
	
	/* ----TRANSACTION DATE EQUALS TODAY ---- */
	private TransactionStatusResponse buildStatusReponseClientOrAtmEqualsToday(TransactionTest transaction) {
		TransactionStatusResponse transStatusResponse = new TransactionStatusResponse();
		transStatusResponse.setReference(transaction.getReference());
		transStatusResponse.setStatus(TransStatus.PENDING);
		transStatusResponse.setAmount(transaction.getAmount().subtract(transaction.getFee()));
		return transStatusResponse;
	}
	private TransactionStatusResponse buildStatusReponseInternalEqualsToday(TransactionTest transaction) {
		TransactionStatusResponse transStatusResponse = new TransactionStatusResponse();
		transStatusResponse.setReference(transaction.getReference());
		transStatusResponse.setStatus(TransStatus.PENDING);
		transStatusResponse.setAmount(transaction.getAmount());
		transStatusResponse.setFee(transaction.getFee());
		return transStatusResponse;
	}

	/* ----TRANSACTION DATE BEFORE TODAY ---- */
	private TransactionStatusResponse buildStatudResponseInternalBeforeToday(TransactionTest transaction) {
		TransactionStatusResponse transStatusResponse = new TransactionStatusResponse();
		transStatusResponse.setReference(transaction.getReference());
		transStatusResponse.setStatus(TransStatus.SETTLED);
		transStatusResponse.setAmount(transaction.getAmount());
		transStatusResponse.setFee(transaction.getFee());
		return transStatusResponse;
	}
	private TransactionStatusResponse buildStatudResponseClientOrAtmBeforeToday(TransactionTest transaction) {
		TransactionStatusResponse transStatusResponse = new TransactionStatusResponse();
		transStatusResponse.setReference(transaction.getReference());
		transStatusResponse.setStatus(TransStatus.SETTLED);
		transStatusResponse.setAmount(transaction.getAmount().subtract(transaction.getFee()));
		return transStatusResponse;
	}

	private TransactionStatusResponse buildStatudResponseTransNotPresent(String reference) {
		TransactionStatusResponse transStatusResponse = new TransactionStatusResponse();
		transStatusResponse.setReference(reference);
		transStatusResponse.setStatus(TransStatus.INVALID);
		return transStatusResponse;
	}

	
	private void checkDateIsNull(Date date) {
		if(date == null) {
			throw new TransactionDateUnprocessableException("Transaction without associated date");
		}
		
	}

	
}
