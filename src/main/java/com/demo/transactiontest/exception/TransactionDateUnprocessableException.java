package com.demo.transactiontest.exception;

import com.demo.transactiontest.exception.generic.UnprocessableException;

public class TransactionDateUnprocessableException extends UnprocessableException {

	private static final long serialVersionUID = 1L;
	private  static final String DESCRIPTION = "Date Unprocessable";
	
	public TransactionDateUnprocessableException(String detail) {super(DESCRIPTION + ". " + detail);}
}
