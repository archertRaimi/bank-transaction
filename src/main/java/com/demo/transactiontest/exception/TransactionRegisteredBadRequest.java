package com.demo.transactiontest.exception;

import com.demo.transactiontest.exception.generic.BadRequestException;

public class TransactionRegisteredBadRequest extends BadRequestException {

	private static final long serialVersionUID = 1L;
	private  static final String DESCRIPTION = "Transaction Bad Request.";
	
	public TransactionRegisteredBadRequest(String detail) {super(DESCRIPTION + ". " + detail);}
}
