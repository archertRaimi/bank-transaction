package com.demo.transactiontest.exception.generic;

public class UnprocessableException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Unprocessable Exception (422)";
	
	public UnprocessableException(String detail) {
		super(DESCRIPTION + ". " + detail);
	}
}
