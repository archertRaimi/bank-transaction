package com.demo.transactiontest.exception;

import com.demo.transactiontest.exception.generic.UnprocessableException;

public class AcountUnprocessableException extends UnprocessableException {

	private static final long serialVersionUID = 1L;
	private  static final String DESCRIPTION = "Iban Unprocessable";
	
	public AcountUnprocessableException(String detail) {super(DESCRIPTION + ". " + detail);}
}
