package com.demo.transactiontest.exception;

import com.demo.transactiontest.exception.generic.BadRequestException;

public class ChannelBadRequestException extends BadRequestException{

	private static final long serialVersionUID = 1L;
	private  static final String DESCRIPTION = "Bad Request to field channel";
	
	public ChannelBadRequestException(String detail) {super(DESCRIPTION + ". " + detail);}
}
