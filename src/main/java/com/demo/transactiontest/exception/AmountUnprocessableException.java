package com.demo.transactiontest.exception;

import com.demo.transactiontest.exception.generic.UnprocessableException;

public class AmountUnprocessableException extends UnprocessableException {

	private static final long serialVersionUID = 1L;
	private  static final String DESCRIPTION = "Amount Unprocessable";
	
	public AmountUnprocessableException(String detail) {super(DESCRIPTION + ". " + detail);}
}
