package com.demo.transactiontest.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.transactiontest.messages.TransactionTestMessages;
import com.demo.transactiontest.requests.TransactionIbanOrderRequest;
import com.demo.transactiontest.requests.TransactionRequest;
import com.demo.transactiontest.requests.TransactionStatusRequest;
import com.demo.transactiontest.services.AccountService;
import com.demo.transactiontest.services.TransactionTestService;

@RestController
public class TransactionTestController {
	
	@Autowired
	private TransactionTestService transService;
	
	@Autowired
	private AccountService accountService;

	@PostMapping("/transactionTest")
	public ResponseEntity<String> addTransaction(@RequestBody @Valid TransactionRequest transactionTestRequest, HttpServletRequest request){
		
		transService.addTransaction(transactionTestRequest);
		return new ResponseEntity<String>(TransactionTestMessages.TRANSACTION_REGISTERED, HttpStatus.OK);
	}

	@PostMapping("/transactionsTest-iban")
	public ResponseEntity<Object> findTransactionByIban(@RequestBody @Valid TransactionIbanOrderRequest transIbanOrderRequest, HttpServletRequest request){
		return new ResponseEntity<Object>(transService.findTransactionsByIban(transIbanOrderRequest), HttpStatus.OK);
	}
	
	@PostMapping("/transactionTest-status")
	public ResponseEntity<Object> statusTransaction(@RequestBody @Valid TransactionStatusRequest transTestStatusRequest, HttpServletRequest request){
		
		return new ResponseEntity<Object>(transService.getStatusTransaction(transTestStatusRequest), HttpStatus.OK);
	}
	
	@GetMapping("/transactionsTest")
	public ResponseEntity<Object> getAll(){
		return new ResponseEntity<Object>(transService.getAll(), HttpStatus.OK);
    }
	
	@PostMapping("/load-account")
	public ResponseEntity<Object> loadAccount(HttpServletRequest request){
		accountService.loadAccount();
		return new ResponseEntity<Object>("Account Loaded", HttpStatus.OK);
	}
	
}
