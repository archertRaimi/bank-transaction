package com.demo.transactiontest.requests;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.demo.transactiontest.messages.TransactionTestMessages;

import lombok.Data;

@Data
public class TransactionRequest {

	private String reference;
	
	@NotBlank(message = TransactionTestMessages.ACCOUNT_IBAN_NOT_BLANK)
	private String account_iban;
	
	private Date date;
	
	@NotNull(message = TransactionTestMessages.AMOUNT_NOT_NULL)
	private BigDecimal amount;
	
	private BigDecimal fee;
	
	private String description;
}
