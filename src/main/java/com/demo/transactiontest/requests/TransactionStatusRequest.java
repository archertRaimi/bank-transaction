package com.demo.transactiontest.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.demo.transactiontest.messages.TransactionTestMessages;

import lombok.Data;

@Data
public class TransactionStatusRequest {

	@NotBlank(message = TransactionTestMessages.REFERENCE_NOT_BLANK)
	private String reference;
	
	@NotBlank(message = TransactionTestMessages.CHANNEL_NOT_VALID)
	@Pattern(regexp="CLIENT|ATM|INTERNAL", message = TransactionTestMessages.CHANNEL_NOT_VALID)
	private String channel;
}
