package com.demo.transactiontest.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.demo.transactiontest.messages.TransactionTestMessages;

import lombok.Data;

@Data
public class TransactionIbanOrderRequest {

	@NotBlank(message = TransactionTestMessages.ACCOUNT_IBAN_NOT_BLANK)
	private String account_iban;
	
	@NotBlank(message = TransactionTestMessages.SORT_NOT_VALID)
	@Pattern(regexp="ASC|DESC", message = TransactionTestMessages.SORT_NOT_VALID)
	private String sort;
}
