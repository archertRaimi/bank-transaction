package com.demo.transactiontest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.demo.transactiontest.converters.TransEntityToTransResponseConverter;
import com.demo.transactiontest.converters.TransRequestToTransEntityConverter;

@Configuration
public class ConverterConfig implements WebMvcConfigurer{

	@Override
    public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new TransRequestToTransEntityConverter());
		registry.addConverter(new TransEntityToTransResponseConverter());
	}
}
