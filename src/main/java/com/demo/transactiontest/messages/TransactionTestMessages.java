package com.demo.transactiontest.messages;

public class TransactionTestMessages {

	public static final String ACCOUNT_IBAN_NOT_BLANK = "Invalid Account Iban. Can't be null or empty";
	public static final String ACCOUNT_IBAN_NOT_EXIST = "The account with that key does not exist";
	public static final String REFERENCE_NOT_BLANK = "Invalid Reference. Can't be null or empty";
	public static final String AMOUNT_NOT_NULL = "Invalid amount. Can't be null";
	public static final String TRANSACTION_REGISTERED = "Transaction registered Successfully";
	public static final String CHANNEL_NOT_VALID = "Invalid channel. Must be CLIENT, ATM, INTERNAL";
	public static final String AMOUNT_UNPROCESSABLE = "Amount cannot be less than 0";
	public static final String SORT_NOT_VALID = "Invalid sort. Must be ASC, DESC";
}
