package com.demo.transactiontest.responses;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class TransactionResponse {

	private String reference;
	
	private String account_iban;
	
	private Date date;
	
	private BigDecimal amount;
	
	private BigDecimal fee;
	
	private String description;
}
