package com.demo.transactiontest.responses;

import java.math.BigDecimal;

import com.demo.transactiontest.enums.TransStatus;

import lombok.Data;

@Data
public class TransactionStatusResponse {

	private String reference;
	
	private TransStatus status;
	
	private BigDecimal amount;
	
	private BigDecimal fee;
	
}
