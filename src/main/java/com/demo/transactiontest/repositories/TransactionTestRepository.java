package com.demo.transactiontest.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.demo.transactiontest.model.TransactionTest;

@Repository
public interface TransactionTestRepository extends JpaRepository<TransactionTest, Long>{

	@Query("SELECT t FROM TransactionTest t WHERE t.account.iban = :iban ORDER BY t.amount ASC ")
	public List<TransactionTest> findTransacctionByIbanOrderAmoutAsc(@Param("iban") String iban);
	
	@Query("SELECT t FROM TransactionTest t WHERE t.account.iban = :iban ORDER BY t.amount DESC ")
	public List<TransactionTest> findTransacctionByIbanOrderAmoutDesc(@Param("iban") String iban);
	
	public Optional<TransactionTest> findByReference(String reference);
}
