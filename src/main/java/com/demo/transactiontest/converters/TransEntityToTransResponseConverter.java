package com.demo.transactiontest.converters;

import org.springframework.core.convert.converter.Converter;

import com.demo.transactiontest.model.TransactionTest;
import com.demo.transactiontest.responses.TransactionResponse;

public class TransEntityToTransResponseConverter implements Converter<TransactionTest, TransactionResponse>{

	@Override
	public TransactionResponse convert(TransactionTest source) {
		TransactionResponse transBkResponse = new TransactionResponse();
		transBkResponse.setReference(source.getReference());
		transBkResponse.setAccount_iban(source.getAccount().getIban());
		transBkResponse.setDate(source.getDate());
		transBkResponse.setAmount(source.getAmount());
		transBkResponse.setFee(source.getFee());
		transBkResponse.setDescription(source.getDescription());
		return transBkResponse;
	}

}
