package com.demo.transactiontest.converters;

import org.springframework.core.convert.converter.Converter;

import com.demo.transactiontest.model.TransactionTest;
import com.demo.transactiontest.requests.TransactionRequest;

public class TransRequestToTransEntityConverter implements Converter<TransactionRequest, TransactionTest>{

	@Override
	public TransactionTest convert(TransactionRequest source) {
		TransactionTest transBK = new TransactionTest();
		transBK.setReference(source.getReference());
		transBK.setDate(source.getDate());
		transBK.setAmount(source.getAmount());
		transBK.setFee(source.getFee());
		transBK.setDescription(source.getDescription());
		return transBK;
	}

}
